FROM nginx

RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/*
ADD nginx.conf /etc/nginx/
ADD conf.d/* /etc/nginx/conf.d/

VOLUME /etc/nginx/ssl
